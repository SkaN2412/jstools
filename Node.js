(function(Node)
{

    Node.attr = function(name, value)
    {
        if ( typeof name == 'undefined' )
        {
            throw 'Expected at least one parameter "attribute name"';
        }

        if ( typeof value == 'undefined' )
        {
            return this.getAttribute(name);
        }
        else
        {
            this.setAttribute(name, value);

            return this;
        }
    };

    Node.removeAttr = function(name)
    {
        this.removeAttribute(name);

        return this;
    };

    Node.hasAttr = function(name)
    {
        return ( this.getAttribute(name) !== null );
    };

    Node.dataAttr = function(name, value)
    {
        return this.attr( 'data-'+name, value );
    };

    Node.css = function(name, value, priority)
    {
        if ( typeof name == 'undefined' )
        {
            throw 'Expected at least one parameter "attribute name"';
        }

        if ( typeof value == 'undefined' )
        {
            return document.defaultView.getComputedStyle(this, null).getPropertyValue(name);
        } else
        {
            var important = priority ? 'important' : null;

            document.getElementsByName('email')[0].style.setProperty('width', '1000px', important);

            return this;
        }
    };

    Node.hasClass = function(name)
    {
        return this.classList.contains(name);
    };

    Node.addClass = function(name)
    {
        this.classList.add(name);

        return this;
    };

    Node.removeClass = function(name)
    {
        this.classList.remove(name);

        return this;
    };

    Node.toggleClass = function(name)
    {
        this.classList.toggle(name);

        return this;
    }

}(Node.prototype));