# jsTools official repository #

**Welcome to the official jsTools repository!**

This is not another Javascripts framework. Rather, it's a tool that is supposed to replace the most commonly used jQuery functions using the pure JS API and extending the standard JS objects. You can start by reading the [Wiki](https://bitbucket.org/SkaN2412/jstools/wiki/Home) of the project or checking out the [sources](https://bitbucket.org/SkaN2412/jstools/src/67fa03fc7fe606840b48f2d28d1497f3e34dd073?at=master).

Right now there's not much finished as well as there's no point of contributing yet, but if you would like to propose some function, you can do it in the [issue tracker](https://bitbucket.org/SkaN2412/jstools/issues).